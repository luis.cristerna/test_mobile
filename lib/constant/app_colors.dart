import 'dart:ui';

class AppColors{

  static Color get primaryColor{
    return const Color(0xff319795);
  }

  static Color get secondaryColor{
    return const Color(0xffFFFFFF);
  }

  static Color get appBarTextColor{
    return const Color(0xff319795);
  }

  static Color get bgMainContainerPrimaryColor{
    return const Color(0xffE6FFFA);
  }

  static Color get bgMainContainerSecondaryColor{
    return const Color(0xffEBF4FF);
  }

  static Color get bgButtonRegisterPrimaryColor{
    return const Color(0xff3182CE);
  }

  static Color get bgButtonRegisterSecondaryColor{
    return const Color(0xff319795);
  }

  static Color get bgSelectedSegmentedControl{
    return const Color(0xff81E6D9);
  }

  static Color get textSegmentedControl{
    return const Color(0xff319795);
  }

  static Color get borderSegmentedControl{
    return const Color(0xffCBD5E0);
  }

  static Color get textTittleControl{
    return const Color(0xff4A5568);
  }

  static Color get textSubTittleControl{
    return const Color(0xff718096);
  }




}