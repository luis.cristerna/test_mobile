import 'package:get/get_navigation/src/routes/get_route.dart';

import '../ui/view/index_page.dart';

appRoutes() => [
  GetPage(
    name: '/index',
    page: () => Index(),
  ),
];