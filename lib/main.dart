import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:responsive_framework/responsive_wrapper.dart';
import 'package:test_mobile/ui/services/color_service.dart';

import 'constant/app_colors.dart';
import 'constant/routes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  final ColorService _colorController = ColorService();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      builder: (context, child) => ResponsiveWrapper.builder(
          child,
          maxWidth: 1500,
          minWidth: 480,
          defaultScale: true,
          breakpoints: [
            const ResponsiveBreakpoint.resize(480, name: MOBILE),
            const ResponsiveBreakpoint.autoScale(800, name: TABLET),
            const ResponsiveBreakpoint.resize(1500, name: DESKTOP),
          ],
          background: Container(color: Color(0xFFF5F5F5))),
      debugShowCheckedModeBanner: false,
      //Initiate Bindings we have created with GETX
      title: 'Deine Job website',
      initialRoute: '/index',
      getPages: appRoutes(),
      theme: ThemeData(
        primarySwatch: _colorController.createMaterialColor(AppColors.primaryColor),
      ),
    );
  }
}
