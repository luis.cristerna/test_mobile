import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proste_bezier_curve/proste_bezier_curve.dart';
import 'package:test_mobile/constant/app_colors.dart';
import 'package:material_segmented_control/material_segmented_control.dart';

import 'dart:math' as math;

class Index extends StatefulWidget {
  const Index({Key? key}) : super(key: key);

  @override
  State<Index> createState() => _IndexState();
}

class _IndexState extends State<Index> {

  int _currentSelection = 0;

  @override
  Widget build(BuildContext context) {
    var screenheight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: AppColors.secondaryColor,
      appBar: AppBar(
        backgroundColor: AppColors.secondaryColor,
        actions: [
          Padding(
            padding: EdgeInsets.only(right: 15, top: 20),
            child: Text('Login', style: TextStyle(color: AppColors.appBarTextColor),),
          ),
        ],
        shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30), bottomRight: Radius.circular(30))),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            const SizedBox(
              height: 3500,
              width: double.infinity,
            ),

            (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
              top: 0,
              left: 0,
              height: 550,
              width: screenWidth,
              child: Stack(
                children: [
                  Container(
                    height: 550,
                    color: AppColors.secondaryColor,
                  ),
                  ClipPath(
                    clipper: ProsteBezierCurve(
                      position: ClipPosition.bottom,
                      list: [
                        BezierCurveSection(
                          start: Offset(0, 550),
                          top: Offset(screenWidth, 500),
                          end: Offset(screenWidth, 500),
                        ),
                      ],
                    ),
                    child: Container(
                      height: 550,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.centerRight,
                          colors: [
                            AppColors.bgMainContainerSecondaryColor,
                            AppColors.bgMainContainerPrimaryColor,
                          ],
                          stops: [0.0, 1.0],
                          transform: GradientRotation(102 * math.pi / 180),
                        ),
                      ),
                    ),
                  ),
                ],
          )
        ) : (MediaQuery.of(context).size.width < 800.0) ?
            Positioned(
              top: 0,
              left: 0,
              height: screenheight,
              width: screenWidth,
              child: Stack(
                children: [
                  Container(
                    height: screenheight,
                    color: AppColors.secondaryColor,
                  ),
                  ClipPath(
                    clipper: ProsteThirdOrderBezierCurve(
                      position: ClipPosition.bottom,
                      list: [
                        ThirdOrderBezierCurveSection(
                          p1: Offset(screenWidth, screenheight - 150),
                          p2: Offset(screenWidth, screenheight - 300),
                          p3: Offset(screenWidth, screenheight - 100),
                          p4: Offset(screenWidth, screenheight - 300),
                        ),
                      ],
                    ),
                    child: Container(
                      height: screenheight,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.centerRight,
                          colors: [
                            AppColors.bgMainContainerSecondaryColor,
                            AppColors.bgMainContainerPrimaryColor,
                          ],
                          stops: [0.0, 1.0],
                          transform: GradientRotation(102 * math.pi / 180),
                        ),
                      ),
                    ),
                  ),
                ],
              )
          ) :
         Positioned(
              top: 0,
              left: 0,
              height: 350,
              width: screenWidth,
              child: Stack(
                children: [
                  Container(
                    height: 350,
                    color: AppColors.secondaryColor,
                  ),
                  ClipPath(
                    clipper: ProsteBezierCurve(
                      position: ClipPosition.bottom,
                      list: [
                        BezierCurveSection(
                          start: Offset(0, 350),
                          top: Offset(screenWidth, 300),
                          end: Offset(screenWidth, 300),
                        ),
                      ],
                    ),
                    child: Container(
                      height: 350,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.centerRight,
                          colors: [
                            AppColors.bgMainContainerSecondaryColor,
                            AppColors.bgMainContainerPrimaryColor,
                          ],
                          stops: [0.0, 1.0],
                          transform: GradientRotation(102 * math.pi / 180),
                        ),
                      ),
                    ),
                  ),
                ],
              )
        ),

         (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
              top: 130,
              right: 450,
              height: 360,
              width: 360,
              child: CircleAvatar(
                backgroundColor: AppColors.secondaryColor,
                backgroundImage: AssetImage('assets/img/undraw_agreement_aajr.png'),
              ),
            ) : (MediaQuery.of(context).size.width < 800.0) ?
           Positioned(
             top: 200,
             height: 600,
             width: screenWidth,
             child: Image.asset('assets/img/undraw_agreement_aajr.png', fit: BoxFit.cover,),
           ) :
           Positioned(
             top: 80,
             right: 250,
             height: 200,
             width: 200,
             child: CircleAvatar(
               backgroundColor: AppColors.secondaryColor,
               backgroundImage: AssetImage('assets/img/undraw_agreement_aajr.png'),
             ),
           ),

           (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
              top: 190,
              left: 300,
              height: 360,
              width: 360,
              child: Text('Deine Job website', style: TextStyle(fontSize: 60, fontWeight: FontWeight.bold),),
            ) : (MediaQuery.of(context).size.width < 800.0) ?
           Positioned(
             top: 30,
             height: 260,
             width: 260,
             child: Text('Deine Job website', style: TextStyle(fontSize: 40), textAlign: TextAlign.center,),
           ) :
           Positioned(
             top: 110,
             left: 100,
             height: 260,
             width: 260,
             child: Text('Deine Job website', style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),),
           ),


           (MediaQuery.of(context).size.width > 800.0) ?
           Positioned(
             top: 370,
             left: 300,
             height: 30,
             width: 300,
             child: buttonRegister(),
           ) : (MediaQuery.of(context).size.width < 800.0) ?
           Positioned(
             top: 30,
             height: 260,
             width: 260,
             child: SizedBox(height: 0,)
           ) :
           Positioned(
             top: 240,
             left: 100,
             height: 30,
             width: 200,
             child: buttonRegister(),
           ),

          (MediaQuery.of(context).size.width > 800.0) ?
           Positioned(
             top: 570,
             //left: 300,
             height: 60,
             width: 500,
             child: segmentedControl(),
           ) : (MediaQuery.of(context).size.width < 800.0) ?
           Positioned(
             top: screenheight + 10,
             height: 60,
             width: 350,
             child: segmentedControl()
           ) :
           Positioned(
             top: 400,
             height: 40,
             width: 400,
             child: segmentedControl(),
           ),

            (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
                top: 1150,
                left: 0,
                height: 350,
                width: screenWidth,
                child: Stack(
                  children: [
                    Container(
                      height: 350,
                      color: AppColors.secondaryColor,
                    ),
                    ClipPath(
                      clipper: ProsteThirdOrderBezierCurve(
                        position: ClipPosition.bottom,
                        list: [
                          ThirdOrderBezierCurveSection(
                            p1: Offset(screenWidth, 350),
                            p2: Offset(screenWidth / 4, 310),
                            p3: Offset(screenWidth / 2, 340),
                            p4: Offset(screenWidth, 285),
                          ),
                        ],
                      ),
                      child: Container(
                        height: 350,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.centerRight,
                            colors: [
                              AppColors.bgMainContainerPrimaryColor,
                              AppColors.bgMainContainerSecondaryColor,
                            ],
                            stops: [0.0, 1.0],
                            transform: GradientRotation(102 * math.pi / 180),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
            ) : (MediaQuery.of(context).size.width < 800.0) ?
            Positioned(
                top: 1650,
                left: 0,
                height: 350,
                width: screenWidth,
                child: Stack(
                  children: [
                    Container(
                      height: 350,
                      color: AppColors.secondaryColor,
                    ),
                    ClipPath(
                      clipper: ProsteThirdOrderBezierCurve(
                        position: ClipPosition.bottom,
                        list: [
                          ThirdOrderBezierCurveSection(
                            p1: Offset(screenWidth, 350),
                            p2: Offset(screenWidth / 4, 310),
                            p3: Offset(screenWidth / 2, 340),
                            p4: Offset(screenWidth, 285),
                          ),
                        ],
                      ),
                      child: Container(
                        height: 350,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.centerRight,
                            colors: [
                              AppColors.bgMainContainerPrimaryColor,
                              AppColors.bgMainContainerSecondaryColor,
                            ],
                            stops: [0.0, 1.0],
                            transform: GradientRotation(102 * math.pi / 180),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
            ) :
            Positioned(
                top: 1150,
                left: 0,
                height: 350,
                width: screenWidth,
                child: Stack(
                  children: [
                    Container(
                      height: 350,
                      color: AppColors.secondaryColor,
                    ),
                    ClipPath(
                      clipper: ProsteThirdOrderBezierCurve(
                        position: ClipPosition.bottom,
                        list: [
                          ThirdOrderBezierCurveSection(
                            p1: Offset(screenWidth, 350),
                            p2: Offset(screenWidth / 4, 310),
                            p3: Offset(screenWidth / 2, 340),
                            p4: Offset(screenWidth, 285),
                          ),
                        ],
                      ),
                      child: Container(
                        height: 350,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.centerRight,
                            colors: [
                              AppColors.bgMainContainerPrimaryColor,
                              AppColors.bgMainContainerSecondaryColor,
                            ],
                            stops: [0.0, 1.0],
                            transform: GradientRotation(102 * math.pi / 180),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
            ),

            (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
                top: 700,
                left: 0,
                height: 580,
                width: screenWidth,
                child: Stack(
                  children: [
                    Container(
                      height: 580,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.centerRight,
                          colors: [
                            AppColors.bgMainContainerPrimaryColor,
                            AppColors.bgMainContainerSecondaryColor,
                          ],
                          stops: [0.0, 1.0],
                          transform: GradientRotation(102 * math.pi / 180),
                        ),
                      ),
                    ),
                    ClipPath(
                      clipper: ProsteThirdOrderBezierCurve(
                        position: ClipPosition.bottom,
                        list: [
                          ThirdOrderBezierCurveSection(
                            p1: Offset(screenWidth, 510),
                            p2: Offset(screenWidth / 4, 580),
                            p3: Offset(screenWidth / 2, 540),
                            p4: Offset(screenWidth, 565),
                          ),
                        ],
                      ),
                      child: Container(
                        height: 580,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                )
            ) :
            (MediaQuery.of(context).size.width < 800.0) ?
            Positioned(
                top: 1150,
                left: 0,
                height: 580,
                width: screenWidth,
                child: Stack(
                  children: [
                    Container(
                      height: 580,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.centerRight,
                          colors: [
                            AppColors.bgMainContainerPrimaryColor,
                            AppColors.bgMainContainerSecondaryColor,
                          ],
                          stops: [0.0, 1.0],
                          transform: GradientRotation(102 * math.pi / 180),
                        ),
                      ),
                    ),
                    ClipPath(
                      clipper: ProsteThirdOrderBezierCurve(
                        position: ClipPosition.bottom,
                        list: [
                          ThirdOrderBezierCurveSection(
                            p1: Offset(screenWidth, 510),
                            p2: Offset(screenWidth / 4, 580),
                            p3: Offset(screenWidth / 2, 540),
                            p4: Offset(screenWidth, 565),
                          ),
                        ],
                      ),
                      child: Container(
                        height: 580,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                )
            ) :
            Positioned(
                top: 700,
                left: 0,
                height: 580,
                width: screenWidth,
                child: Stack(
                  children: [
                    Container(
                      height: 580,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.centerRight,
                          colors: [
                            AppColors.bgMainContainerPrimaryColor,
                            AppColors.bgMainContainerSecondaryColor,
                          ],
                          stops: [0.0, 1.0],
                          transform: GradientRotation(102 * math.pi / 180),
                        ),
                      ),
                    ),
                    ClipPath(
                      clipper: ProsteThirdOrderBezierCurve(
                        position: ClipPosition.bottom,
                        list: [
                          ThirdOrderBezierCurveSection(
                            p1: Offset(screenWidth, 510),
                            p2: Offset(screenWidth / 4, 580),
                            p3: Offset(screenWidth / 2, 540),
                            p4: Offset(screenWidth, 565),
                          ),
                        ],
                      ),
                      child: Container(
                        height: 580,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                  ],
                )
            ),

            (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
              top: 670,
              //left: 300,
              height: 160,
              width: 400,
              child: Text(getTittleText(), style: TextStyle(color: AppColors.textTittleControl,fontSize: 35, fontWeight: FontWeight.normal), textAlign: TextAlign.center,),
            ) : (MediaQuery.of(context).size.width < 800.0) ?
            Positioned(
              top: screenheight + 100,
              //left: 300,
              height: 160,
              width: 400,
              child: Text(getTittleText(), style: TextStyle(color: AppColors.textTittleControl,fontSize: 20, fontWeight: FontWeight.normal), textAlign: TextAlign.center,),
            ) :
            Positioned(
              top: 570,
              //left: 300,
              height: 160,
              width: 400,
              child: Text(getTittleText(), style: TextStyle(color: AppColors.textTittleControl,fontSize: 35, fontWeight: FontWeight.normal), textAlign: TextAlign.center,),
            ),


          (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
              top: 870,
              left: 300,
              height: 160,
              width: 100,
              child: Text("1.", style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 75, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ) : (MediaQuery.of(context).size.width < 800.0) ?
            Positioned(
              top: screenheight + 400,
              left: 50,
              height: 160,
              width: 100,
              child: Text("1.", style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 75, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ) :
            Positioned(
              top: 870,
              left: 100,
              height: 160,
              width: 100,
              child: Text("1.", style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 75, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ),

           (MediaQuery.of(context).size.width > 800.0) ?
             Positioned(
               top: 915,
               left: 400,
               height: 160,
               width: 800,
               child: Text(getSubTittle1Text(), style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 25, fontWeight: FontWeight.bold), textAlign: TextAlign.left,),
             ) : (MediaQuery.of(context).size.width < 800.0) ?
           Positioned(
             top: screenheight + 450,
             left: 140,
             height: 160,
             width: 300,
             child: Text(getSubTittle1Text(), style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 25, fontWeight: FontWeight.bold), textAlign: TextAlign.left,),
           ) :
            Positioned(
               top: 915,
               left: 200,
               height: 160,
               width: 800,
               child: Text(getSubTittle1Text(), style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 25, fontWeight: FontWeight.bold), textAlign: TextAlign.left,),
             ),

        (MediaQuery.of(context).size.width > 800.0) ?
             Positioned(
               top: 825,
               left: 600,
               height: 160,
               width: 800,
               child: Image.asset('assets/img/undraw_Profile_data_re_v81r.png'),
             ) : (MediaQuery.of(context).size.width < 800.0) ?
          Positioned(
            top: screenheight + 240,
            //left: 150,
            height: 160,
            width: 800,
            child: Image.asset('assets/img/undraw_Profile_data_re_v81r.png'),
          ) :
          Positioned(
              top: 740,
              //left: 150,
              height: 160,
              width: 800,
              child: Image.asset('assets/img/undraw_Profile_data_re_v81r.png'),
            ),

          (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
              top: 825,
              left: 600,
              height: 160,
              width: 800,
              child: Image.asset('assets/img/undraw_Profile_data_re_v81r.png'),
            ) : (MediaQuery.of(context).size.width < 800.0) ?
            Positioned(
              top: screenheight + 240,
              //left: 150,
              height: 160,
              width: 800,
              child: Image.asset('assets/img/undraw_Profile_data_re_v81r.png'),
            ) :
            Positioned(
              top: 740,
              //left: 150,
              height: 160,
              width: 800,
              child: Image.asset('assets/img/undraw_Profile_data_re_v81r.png'),
            ),

            (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
              top: 1300,
              left: 100,
              height: 160,
              width: 800,
              child: Image.asset('assets/img/undraw_task_31wc.png'),
            ) : (MediaQuery.of(context).size.width < 800.0) ?
            Positioned(
              top: screenheight + 790,
              //left: 150,
              height: 160,
              width: 800,
              child: Image.asset('assets/img/undraw_task_31wc.png'),
            ) :
            Positioned(
              top: 1300,
              right: 200,
              height: 160,
              width: 800,
              child: Image.asset('assets/img/undraw_task_31wc.png'),
            ),

            (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
              top: 1350,
              left: 650,
              height: 160,
              width: 100,
              child: Text("2.", style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 75, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ) : (MediaQuery.of(context).size.width < 800.0) ?
            Positioned(
              top: screenheight + 700,
              left: 50,
              height: 160,
              width: 100,
              child: Text("2.", style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 75, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ) :
            Positioned(
              top: 1350,
              left: 330,
              height: 160,
              width: 100,
              child: Text("2.", style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 75, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
            ),

            (MediaQuery.of(context).size.width > 800.0) ?
            Positioned(
              top: 1390,
              left: 750,
              height: 160,
              width: 800,
              child: Text(getSubTittle1Text(), style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 25, fontWeight: FontWeight.bold), textAlign: TextAlign.left,),
            ) : (MediaQuery.of(context).size.width < 800.0) ?
            Positioned(
              top: screenheight + 730,
              left: 140,
              height: 160,
              width: 300,
              child: Text(getSubTittle1Text(), style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 25, fontWeight: FontWeight.bold), textAlign: TextAlign.left,),
            ) :
            Positioned(
              top: 1390,
              left: 430,
              height: 160,
              width: 800,
              child: Text(getSubTittle1Text(), style: TextStyle(color: AppColors.textSubTittleControl,fontSize: 25, fontWeight: FontWeight.bold), textAlign: TextAlign.left,),
            ),

          ],
        ),
      ),
    );
  }

  Map<int, Widget> _children = {
    0: Text('Arbeitnehmer'),
    1: Text('Arbeitgeber'),
    2: Text('Temporärbüro'),
  };

  Widget buttonRegister(){
    return Container(
      height: 44.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          gradient: LinearGradient(colors: [AppColors.bgButtonRegisterSecondaryColor, AppColors.bgButtonRegisterPrimaryColor])
      ),
      child: ElevatedButton(
        onPressed: () {},
        style: ElevatedButton.styleFrom(primary: Colors.transparent, shadowColor: Colors.transparent),
        child: Text('Kostenlos Registrieren', style: TextStyle(color: AppColors.secondaryColor, fontWeight: FontWeight.normal),),
      ),
    );
  }

  Widget segmentedControl(){
    return MaterialSegmentedControl(
      children: _children,
      selectionIndex: _currentSelection,
      borderColor: AppColors.borderSegmentedControl,
      selectedColor: AppColors.bgSelectedSegmentedControl,
      unselectedColor: AppColors.secondaryColor,
      borderRadius: 10.0,
      disabledChildren: [
        3,
      ],
      onSegmentChosen: (index) {
        setState(() {
          _currentSelection = index;
        });
      },
    );
  }

  String getTittleText(){
    switch (_currentSelection) {
      case 0:
        return 'Drei einfache Schritte zu deinem neuen Job';
      case 1:
        return 'Drei einfache Schritte zu deinem neuen Mitarbeiter';
      case 2:
        return 'Drei einfache Schritte zur Vermittlung neuer Mitarbeiter';
      default:
        return 'Drei einfache Schritte zu deinem neuen Job';
    }
  }

  String getSubTittle1Text(){
    switch (_currentSelection) {
      case 0:
        return 'Erstellen dein Lebenslauf';
      case 1:
        return 'Erstellen dein Unternehmensprofil';
      case 2:
        return 'Erstellen dein Unternehmensprofil';
      default:
        return 'Erstellen dein Lebenslauf';
    }
  }


}
